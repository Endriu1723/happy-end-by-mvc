package com.cusbee.model;

import org.springframework.stereotype.Component;

import java.io.File;
@Component
public class FileFinder {

    private static String findingfile;
    private String first ;
    private String second ;
    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }
    public static void findFile(String fileName, File directory) {    //крч це є конструктор для каталога
        findingfile = fileName;
        File file = findFile(directory);
        if (file == null) {
            System.out.println("Файла нет");
        } else {
            System.out.println("Путь к файлу: " + file.getAbsolutePath());  //  абсолютный путь файла(getAbsolutePath())
        }
    }
    private static File findFile(File directory) {
        for (File file : directory.listFiles())
            System.out.println(file.getPath());   //getPath путь
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {     // isDirectiry  каталог?    isFile файл??
                File findedFile = findFile(file);
                if (findedFile != null) {
                    return findedFile;
                }
            } else {
                if (file.getName().contains(findingfile)) {
                    return file;
                }
            }
        }
        return null;
    }
    public static void main(String[] args) {
        String first = "pam.dll";
        String second = "C://Program Files";
        FileFinder.findFile(first, new File(second));
    }


}