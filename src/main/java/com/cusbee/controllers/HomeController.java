package com.cusbee.controllers;


import com.cusbee.model.FileFinder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller //пишемо що це є констроллер
        public class HomeController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("fileJSP", new FileFinder());
        modelAndView.setViewName("welcome");

        return modelAndView;
    }

    @RequestMapping(value = "/file-cool")
    public ModelAndView fileCool(@ModelAttribute("fileJSP") FileFinder fileFinder) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("secondPage");
        modelAndView.addObject("file", fileFinder);

        return modelAndView; //после уйдем на представление, указанное чуть выше, если оно будет найдено.
    }
}