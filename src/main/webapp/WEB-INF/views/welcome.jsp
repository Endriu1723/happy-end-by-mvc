<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Welcome</title>
</head>
<body>
<spring:form method="post"  modelAttribute="fileJSP" action="file-cool">
    File: <spring:input path="first"/>  <br/>
    Katalog: <spring:input path="second"/>   <br/>
    <spring:button>Next Page</spring:button>
</spring:form>
</body>
</html>